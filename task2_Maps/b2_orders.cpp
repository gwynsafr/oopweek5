#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

void print_customer_map(map<string, map<string,int>> mps) {
    for (const auto& mp : mps) {
        std::cout << mp.first << ":\n";
        for (const auto& smoll_map : mp.second) {
        std::cout << smoll_map.first << ' ' << smoll_map.second << '\n';
        }
    }
}

map<string, map<string,int>> create_customer_map() {
    vector<string> words;
    string word;
    string line;
    map<string, map<string,int>> customer_orders;
    while (std::getline(std::cin, line)) {
        if (line == "") return customer_orders;
        stringstream iss(line);
        while (iss >> word) {
            words.push_back(word);
        }
        if (customer_orders[words[0]][words[1]]) {
            customer_orders[words[0]][words[1]] += std::stoi(words[2]);
        }
        else {
            customer_orders[words[0]][words[1]] = std::stoi(words[2]);
            }
        words.clear();
    }
}

int main()
{
    print_customer_map(create_customer_map());
    return 0;
}
