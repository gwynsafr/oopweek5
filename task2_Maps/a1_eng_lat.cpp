#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <vector>
#include <set>
using namespace std;

void print_dict(map<string, vector<string>> dict) {
    for (auto mp : dict) {
        std::cout << mp.first << " - ";
        for (std::string word : mp.second) {
            std::cout << word << ' ';
        }
        std::cout << '\n';
    }
}

void print_rdict(map<string, set<string>> dict) {
    for (auto mp : dict) {
        std::cout << mp.first << " - ";
        for (std::string word : mp.second) {
            std::cout << word << ' ';
        }
        std::cout << '\n';
    }
}

map<string, vector<string>> create_eng_lat_dict() {
    vector<string> words;
    string word;
    string line;
    map<string, vector<string>> eng_lat_dict;
    while (std::getline(std::cin, line)) {
        if (line == "") return eng_lat_dict;
        stringstream iss(line);
        while (iss >> word) {
            auto pos = (word.find(","));
            if (pos != std::string::npos) {
               word.erase(pos);
            }
            words.push_back(word);
        }
        auto it = words.begin();
        ++it;
        ++it;
        for (auto end=std::end(words); it!=end; ++it) {
            eng_lat_dict[words[0]].push_back(*it);
        }
        words.clear();
    }
}

map<string, set<string>> create_reverse_dict(map<string, vector<string>> dict) {
    map<string, set<string>> new_dict;
    for (auto mp : dict) {
        for (std::string word : mp.second) {
            new_dict[word].insert(mp.first);
        }
    }
    return new_dict;
}

int main() {
    print_rdict(create_reverse_dict(create_eng_lat_dict()));
    return 0;
}
