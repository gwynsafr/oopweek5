#include <iostream>
#include <map>
#include <vector>
#include <iomanip>
#include <algorithm>

int classicfib(int amount) {
    // Number too big
    if (amount >= 47) {
        return -1;
    }
    if (amount < 3) {
        return 1;
    } else {
        return classicfib(amount - 1) + classicfib(amount - 2);
    }
}

int fibwithcash(int amount) {
    if (amount >= 47) {
        return -1;
    }
    std::map<int, int> numbers;
    if (amount < 0) {
        return -1;
    }
    for (int i=0; i < 2; i++) {
        numbers[i] = 1;
    }
    for (int i=2; i < amount; i++) {
        numbers[i] = numbers[i-1] + numbers[i-2];
    }
    return numbers[amount - 1];
}

double get_median(std::vector<double> finish_times) {
    std::sort(finish_times.begin(), finish_times.end());
    return finish_times[finish_times.size() / 2];
}

double get_run_time(int (*timethis)(int), int amount, int precition=100) {
    std::vector<double> finish_times;
    clock_t start, end;
    for (int i = 0; i < precition; i++) {
        start = clock();
        timethis(amount);
        end = clock();
        double time_taken = double(end - start) / double(CLOCKS_PER_SEC);
        finish_times.push_back(time_taken);
        }
    return get_median(finish_times);
}

void get_compare_times(int this_much_dots) {
    std::vector<long double> cash,
                        classic;
    for (int dot_i = 0; dot_i < this_much_dots; dot_i++) {
        cash.push_back(get_run_time(&fibwithcash, dot_i));
        classic.push_back(get_run_time(&classicfib, dot_i));

    }
    std::cout << "С Кешем: ";
    for (auto time : cash) {
        std::cout << time << std::setprecision(12) << " ";
    }
    std::cout << '\n' << "Рекурсия: ";
    for (auto time : classic) {
        std::cout << time << std::setprecision(12) << " ";
    }
}


int main() {
    std::cout << "Please, input amount of desired numbers\n";
    int amount;
    std::cin >> amount;
    std::cout << fibwithcash(amount) << '\n';
    std::cout << classicfib(amount) << '\n';
    //get_compare_times(46);
    return 0;
}
