#include <iostream>
#include <list>
using namespace std;

int get_value(list<int> nums, int index) {
    // Возвращает значение из list по индексу
    int count_index = 0;
    for (int const i : nums) {
        if (index == count_index) {
            return i;
        }
        count_index++;
    }
}

int max_difference(int value, list<int> nums) {
    // Находит максимальную разницу между проверяемым значением и всеми последующими
    int max_diff = 0,
        temp1;
    int static checking = 0;
    for (int i = checking; i < nums.size() - 1; i++) {
        temp1 = get_value(nums, i) - value;
        if (temp1 > max_diff) {
            max_diff = temp1;
        }
    }
    checking++;
    return max_diff;
}

int calc_profit(list<int> nums) {
    if (nums.size() <= 1) {
        return -1;
    }
    // Расчитывает максимальную прибыль
    if (nums.empty()) {
        return -1;
    }
    int temp2,
        profit = 0;
    for (int current_value : nums) {
        temp2 = max_difference(current_value, nums);
        if (temp2 > profit) {
            profit = temp2;
        }
    }
    return profit;
}

int main() {
    cout << "Укажите длину списка и заполните его: ";
    int list_length;
    cin >> list_length;
    list<int> nums;
    for (int x, i = 0; i < list_length && cin >> x; i++) {
        nums.push_back(x);
    }
    cout << calc_profit(nums) << '\n';
}
