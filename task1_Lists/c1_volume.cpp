#include <iostream>
#include <sstream>
#include <list>
using namespace std;

list<int> extractIntegerWords(string str) {
    if (str.size() < 1) {
        return {};
    }
    list<int> nums;
    stringstream ss;
    ss << str;
    string temp;
    int found;
    while (!ss.eof()) {
        ss >> temp;
        if (stringstream(temp) >> found)
            nums.push_back(found);
        temp = "";
    }
    return nums;
}

int get_max_volume(list<int> nums) {
    if (nums.size() < 2) {
        return -1;
    }
    int max_volume = 0;
    int current_index = 0;
    int x_value = -1;

    auto it = std::begin(nums);
    for (int value : nums) {
        it = std::begin(nums);
        for (int i=0; i < current_index; i++) {
            ++it;
            x_value++;
        }
        for (auto end=std::end(nums); it!=end; ++it) {
            x_value++;
            if (*it <= value) {
                max_volume = max(max_volume, (x_value-current_index)**it);
            }

        }
    current_index++;
    x_value = -1;
    }
    return max_volume;
}

int main()
{
    std::cout << "Please, input numbers divided by space\n";
    std:: string str;
    std::getline(std::cin, str);
    std::cout << get_max_volume(extractIntegerWords(str));
    return 0;
}
