#include <algorithm>
#include <iostream>
#include <list>
#include <string>
using namespace std;

list<int> create_list(int list_length) {
    list<int> nums;
    for (int x, i = 0; i < list_length && cin >> x; i++) {
        nums.push_back(x);
    }
    return nums;
}

void print_list_as_table(std::list<int> nums) {
    
    for (int value : nums) {
        std::cout << value << ' ';
    }
    std::cout << '\n';
}

void print_list_of_lists(list<list<int>> lst) {
    std::string print_that = "[";
    for (list<int> smol_lst : lst) {
        print_that += '[';
        for (int value : smol_lst) {
            print_that += std::to_string(value) + ','; 
        }
        print_that.pop_back();
        print_that += "], ";
    }
    print_that.pop_back();
    print_that.pop_back();
    std::cout << print_that + "]\n";
}

std::list<std::list<int>> perms(std::list<int> nums) {
    nums.sort();
    list<list<int>> perms_list;
    perms_list.push_back(nums);
    while (next_permutation(nums.begin(), nums.end())) {
        perms_list.push_back(nums);
    }
    return perms_list;
}

int main()
{
    int list_length;
    cout << "Укажите длину списка и заполните его: ";
    cin >> list_length;
    print_list_of_lists(perms(create_list(list_length)));
    return 0;
}
