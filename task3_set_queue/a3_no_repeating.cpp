/* Set, Queue
Написать программу, которая удаляет дубликаты из входного массива.
*/
#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>

int main() {
    int length = 0;
    std::cout << "Input array length: ";
    std::cin >> length;
    std::set<int> s;

    std::copy_n(std::istream_iterator<int>(std::cin), length, std::inserter(s, s.begin()));

    for(auto v : s) std::cout << v << ' ';
}
