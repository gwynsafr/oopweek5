#include <iostream>
#include <set>
#include <string>
#include <map>

std::map<std::string, int> create_languages_map() {
    // Create initial map
    int students,
        student_knows;
    std::map <std::string, int> mp;
    std::cin >> students;
    std::string temp_str; 
    for (int stud = 0; stud < students; stud++) {
        std::cin >> student_knows;
        for (int i = 0; i < student_knows; i++) { 
            std::cin >> temp_str;
            if (mp[temp_str]) {
                mp[temp_str]++;
            }
            else {
                mp[temp_str] = 1;
            }
        }   
    }
    return mp;
}

void print_one_number(int value, std::set<std::string> lang_s) {
    // Print results for one amount
    std::string constructed_std = "";
    for (const std::string language: lang_s) {
        constructed_std += language + "\n";
    }
    std::cout << value << '\n' << constructed_std;
}

void get_results(std::map <std::string, int> mp) {
    // Main Thing, probably should split it up, but deadline is coming for me. HELP!!!
    int max_known_language = 0;
    std::set<int> values_s;
    std::set<int>::reverse_iterator rit;
    std::set<std::string> lang_s;
    
    for (const auto n : mp) {
    // Записываем все количества, для ориентира
        values_s.insert(n.second);
    }
   for (rit = values_s.rbegin(); rit != values_s.rend(); rit++) {
        for (const auto n : mp) {
            if (*rit == n.second) {
                lang_s.insert(n.first);
            }
        }
        print_one_number(*rit, lang_s);
        lang_s.clear();
    }
}

int main() {
    std::map <std::string, int> mp =  create_languages_map();
    get_results(mp);
    return 0;
}
