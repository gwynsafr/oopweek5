/* Set, Queue
На вход программы подается строка, содержащая только символы '(', ')', '{', '}', '[' и ']', определите, валидна ли входная строка.
Входная строка действительна, если:
1. Открытые скобки должны быть закрыты однотипными скобками.
2. Открытые скобки должны быть закрыты в правильном порядке.
3. Каждой закрывающей скобке соответствует открытая скобка того же типа.
Реализуйте метод isValid, возвращающий тип boolean. Тип и необходымые входные параметры метода предложите самостоятельно. 
*/

#include <iostream>
#include <queue>
#include <string>
#include <algorithm>

std::string only_parentheses_left(const std::string &to) {
    std::string final_string;
    for(std::string::const_iterator it = to.begin(); it != to.end(); ++it) {
        if((*it >= '0' && *it <= '9') || (*it >= 'a' && *it <= 'z') || (*it >= 'A' && *it <= 'Z') || *it == '_') {
        }
        else {
            final_string += *it;
        }
    }
    return final_string;
}

bool IsValid(std::string input) {
    std::string input_string = only_parentheses_left(input);
    if (input_string.size() < 2) {
        std::cout << "Invalid string length\n";
        return -1;
    }
    std::queue <char> q;
    char prev_sym;
    for (char sym : input_string) {
        switch (sym) {
            case '(':
            case ')':
            case '{':
            case '}':
            case '[':
            case ']':
                break;
            default:
                std::cout << "Invalid string\n";
                return -1;
        }
        q.push(sym);
    }
    prev_sym = q.front();
    q.pop();
    while (!q.empty()) {
        switch(prev_sym) {
            case ')':
            case ']':
            case '}':
                break;
            case '(':
                if (q.front() - prev_sym != 1) {
                    return false;
                }
                break;
            case '{':
                if (q.front() - prev_sym != 2) {
                    return false;
                }
                break;
            case '[':
                if (q.front() - prev_sym != 2) {
                    return false;
                }
                break;
            default:
                return false;
        }
        prev_sym = q.front();
        q.pop();
    }
    return true;
}

int main() {
    std::string input_string;
    std::cin >> input_string;
    std::cout << IsValid(input_string) << '\n';
    return 0;
}
